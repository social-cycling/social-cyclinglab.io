---
title: Artículo publicado en Frontiers in Sustainable Cities
date: 2020-07-24
category: Blog
authors: Rodrigo Garcia-Herrera, Paola Garcia-Meneses
---

Publicamos nuestro diseño y las simulaciones descritas en [este
trabajo](https://gitlab.com/rgarcia-herrera/social-cycling/-/blob/master/README.md) en el artículo

- [Social Cycling: Critical Mass Through a Mobile App](https://www.frontiersin.org/articles/10.3389/frsc.2020.00036/full)


incluido en la sección [Urban Transportation Systems and
Mobility](https://www.frontiersin.org/journals/sustainable-cities/sections/urban-transportation-systems-and-mobility)
de la revista [Frontiers in Sustainable Cities](https://www.frontiersin.org/journals/sustainable-cities).

