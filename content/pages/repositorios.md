---
Title: Software
---

# Flockompass

Cliente para android, desarrollado en [kivy](https://kivy.org).
Colabora con el desarrollo de
[Flockompass](https://gitlab.com/social-cycling/flockompass).

# Flock Server

Implementación de referencia de un servidor de enjambres.
Colabora con el desarrollo del [Flock Server](https://gitlab.com/social-cycling/flock-server).
