---
title: Social Cycling
---

Pedalear en grupo es más seguro y más divertido.

 - Aumenta la visivilidad de todos.

 - Pueden afirmar más fácilmente su derecho a la vía, ocupando todo el carril.

 - Si alguien del grupo tiene un desperfecto con su bici puede contar
   con la ayuda inmediata de sus compañeros de viaje.

Social Cycling ayuda a coordinar ciclistas para dar lugar a grupos o
caravanas en todos lados, todo el tiempo.



## Masa crítica casual


Social Cycling te ayuda a superar tu invividualidad. Tratándose del
cuidado del ambiente, en las palabras de [Annie Leonard](https://www.storyofstuff.org/): «las acciones
individuales son un buen lugar para empezar, pero un pésimo lugar para
terminar». Es necesario ligarlas a más acciones políticas, a visiones
más grandes y campañas más atrevidas.

Una tal acción política es la reunión mensual Critical Mass, en
español Masa Crítica, que ocurre en muchas ciudades del mundo. Reune a
decenas de ciclistas que, gracias a sus números, pueden rodar
libremente por la ciudad, celebrando la cultura de la bici y
reclamando su derecho a compartir las calles.

Moverse en bicicleta es una elección individual de tránsito. Moverse
en caravanas de ciclistas crea la masa crítica.




## Gobernanza horizontal

El sistema que estamos desarrollando aumentará la seguridad de
ciclistas urbanas forzadas a compartir el camino con automóviles y
otros vehículos motorizados.

Es una propuesta bottom-up.

 - La auto-organización de caravanas ocurre desde las interacciones
   locales, como una propiedad emergente de la inteligencia
   distribuida entre los usuarios.

 - La interacción de elecciones individuales de ciclistas se
   auto-organizan formando caravanas. Los ciclistas mismos vigilan su
   seguridad, usando su fuerza en números, sin intervención de las
   autoridades. Cambiar el patrón de uso es una forma de intervención
   temporal y efímera de la red de calles.


Aumentar la seguridad es un problema «perverso», que requiere
intervenciones a cargo de múltiples actores en diferentes
escalas. Documentos como la guía [Más ciclistas, más seguros](http://mexico.itdp.org/wp-content/uploads/MasCiclistas_MasSeguros.pdf)
describen intervenciones top-down, del tipo que sólo puede llevar a
cabo el gobierno de una ciudad, pues se trata del diseño de calles,
del trazo de carriles confinados, de la aplicación de leyes.

Tener infraestructura urbana más segura para pedalear está muy bien, y
puede lograrse a través de las autoridades. Formar caravanas para
aumentar la seguridad al pedalear es una medida que podemos tomar
nosotros, directamente.
